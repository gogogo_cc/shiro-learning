package com.zpark.shiro_jsp.mapper;

import com.zpark.shiro_jsp.domain.Role;
import com.zpark.shiro_jsp.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Title: UserMapper
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/1 7:11
 * desc:
 */
@Mapper
public interface UserMapper {
    void save(User user);
    User findByUsername(String username);
    User findRolesByUserName(@Param("username") String username);
}
