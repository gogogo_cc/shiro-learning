package com.zpark.shiro_jsp.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Title: User
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/1 7:09
 * desc:
 */
@Data
public class User {
    private Integer id;
    private String username;
    private String password;
    private String salt;
    //定义角色集合
    private List<Role> roles;
}
