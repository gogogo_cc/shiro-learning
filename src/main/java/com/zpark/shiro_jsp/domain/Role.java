package com.zpark.shiro_jsp.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Title: Role
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/2 6:30
 * desc:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role {
    private Integer id;
    private String name;
    //定义权限的集合
    private List<Perms> perms;
}
