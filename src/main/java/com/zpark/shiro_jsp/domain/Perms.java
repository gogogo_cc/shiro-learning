package com.zpark.shiro_jsp.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Title: Role
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/2 6:30
 * desc:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Perms {
    private Integer id;
    private String name;
    private String url;
    //
}
