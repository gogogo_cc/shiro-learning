package com.zpark.shiro_jsp.service;

import com.zpark.shiro_jsp.domain.User;

import java.util.List;

/**
 * @Title: UserService
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/1 7:14
 * desc:
 */
public interface UserService {
    void register(User user);
    User findByUsername(String username);
    User findRolesByUserName(String username);

}
