package com.zpark.shiro_jsp.service.impl;

import com.zpark.shiro_jsp.domain.User;
import com.zpark.shiro_jsp.mapper.UserMapper;
import com.zpark.shiro_jsp.service.UserService;
import com.zpark.shiro_jsp.uitl.SaltUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Title: UserServiceImpl
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/1 7:15
 * desc:
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public void register(User user) {
        //明文md5+salt+hash散列
       //生成随机盐
        String salt= SaltUtils.getSalt(6);
        user.setSalt(salt);
        Md5Hash md5Hash = new Md5Hash(user.getPassword(),salt,1024);
        user.setPassword(md5Hash.toHex());
        //给用户密码加盐 然后设置一下参数即可
        userMapper.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }

    @Override
    public User findRolesByUserName(String username) {
        return  userMapper.findRolesByUserName(username);
    }
}
