package com.zpark.shiro_jsp.realm;

import com.zpark.shiro_jsp.domain.User;
import com.zpark.shiro_jsp.service.UserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Title: CustomerRealm
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/5/31 22:10
 * desc:
 */
public class CustomerRealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;

    //授权
    //获取认证信息方法
    //认证通过才执行授权的
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        //获取身份信息
        String primaryPrincipal = (String) principals.getPrimaryPrincipal();
        //根据主身份信息获取角色和权限信息
        System.out.println("primaryPrincipal="+primaryPrincipal);
        User user = userService.findRolesByUserName(primaryPrincipal);
        System.out.println("获取用户的角色信息={}"+user.toString());
        //获取授权角色信息
        if (!CollectionUtils.isEmpty(user.getRoles())) {
            SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
            user.getRoles().forEach(role -> {
                System.out.println("name=" + user.getUsername() + "添加角色=" + role.getName());
                simpleAuthorizationInfo.addRole(role.getName());
            });
            return simpleAuthorizationInfo;
        }
       /* if("lisi".equals(primaryPrincipal)){
            SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
            simpleAuthorizationInfo.addRole("admin");
              //simpleAuthorizationInfo.addRole("user");
//            资源操作符：操作类型：操作实例
            //simpleAuthorizationInfo.addStringPermission("user:*:*");
//            simpleAuthorizationInfo.addStringPermission("user:add:*");
          //  simpleAuthorizationInfo.addStringPermission("admin:update:*");
           // simpleAuthorizationInfo.addStringPermission("user:update:*");
            return simpleAuthorizationInfo;
        }*/
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String principal = (String) token.getPrincipal();
        System.out.println(token.getCredentials());
        System.out.println("authenticationInfo" + principal);
        // UserService userServiceImpl = (UserService) ApplicationContextUtils.getBean("userServiceImpl");
        User user1 = userService.findByUsername(principal);
        if (user1 != null) {
            return new SimpleAuthenticationInfo(user1.getUsername(), user1.getPassword(), ByteSource.Util.bytes(user1.getSalt()), this.getName());
        }
        return null;
    }
}
