package com.zpark.shiro_jsp.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title: OrderController
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/1 22:39
 * desc:
 */
@RestController
@RequestMapping("order")
public class OrderController {

    //通过代码的方式进行权限的控制
    @RequestMapping("save")
    //@RequiresRoles(value={"admin","user"}) //t同时具有
    @RequiresRoles(value={"admin"}) //t同时具有
    // @RequiresPermissions("user:update:01")
    public String save() {
        System.out.println("进入save");
        //获取主体对象
        Subject subject = SecurityUtils.getSubject();
        if (subject.hasRole("admin")) {
            System.out.println("保存订单");
        } else {
            System.out.println("无权访问");
        }
        if(subject.isPermitted("admin:add:*")){
            System.out.println("have admin:user:* permission");
        } else {
            System.out.println("no permission");
        }
        return "index";
    }
}
