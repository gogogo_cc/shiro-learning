package com.zpark.shiro_jsp.uitl;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @Title: ApplicationContext
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/1 7:42
 * desc:
 */
//@Component
public class ApplicationContextUtils implements ApplicationContextAware {
    private static ApplicationContextUtils context;
    @Override
    public void setApplicationContext(org.springframework.context.ApplicationContext applicationContext) throws BeansException {
       this.context= (ApplicationContextUtils) applicationContext;

    }
    //根据名字从工厂中获取bean
    public static Object getBean(String beanName){
        return context.getBean(beanName);
    }
}
