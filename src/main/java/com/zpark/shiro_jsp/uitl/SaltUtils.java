package com.zpark.shiro_jsp.uitl;

import java.util.Random;

/**
 * @Title: SaltUtils
 * @Description:
 * @Author : 黎基彩
 * Version: 1.0
 * create: 2021/6/1 7:16
 * desc:
 */
public class SaltUtils {
    /*生成salt*/
    public static String getSalt(int n){
        char[] chars = "iudsdhu56841".toCharArray();
       StringBuilder ab=new StringBuilder();
        for(int i=0;i<n;i++){
          char c= chars[new Random().nextInt(chars.length)];
          ab.append(c);
        }
        return ab.toString();
    }

}
